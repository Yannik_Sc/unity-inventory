namespace Inventory {
    enum EnumToolMaterial {
        WOOD,

        STONE,

        BONE,

        COPPER,

        STEEL
    }
}