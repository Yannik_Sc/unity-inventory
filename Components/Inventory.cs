using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Inventory {
    public class Inventory : MonoBehaviour {
        public GameObject uiItemPrefab;

        public GameObject uiInventory;

        public int inventorySize = 9;

        protected List<ItemBase> items = new List<ItemBase> ();

        protected int activeItem = 0;

        //
        // Summary:
        //     Adds the given item to the inventory stack
        // Parameter:
        //   item
        //     The item which should be added
        public bool AddItem (ItemBase item) {
            if (this.IsFull ()) return false;

            this.items.Add (item);

            GameObject uiItem = Object.Instantiate (this.uiItemPrefab, new Vector3 (0, 0, 0), Quaternion.identity, this.uiInventory.transform);

            RectTransform transform = uiItem.GetComponent<RectTransform> ();
            transform.anchoredPosition3D = new Vector3 (-50 + (100 * this.items.Count), -50, 0);

            GameObject uiImage = uiItem.transform.GetChild (0).gameObject;
            uiImage.GetComponent<RawImage> ().texture = item.itemTexture;

            return true;
        }

        public bool IsFull () {
            return this.items.Count == this.inventorySize;
        }

        //
        // Summary:
        //     Calls the "OnUse" method for the item in Hand
        public void UseActiveItem () {
            if (this.items.Count == 0) return;

            this.SetActiveItem (this.activeItem);

            ItemBase item = this.items[this.activeItem];

            item.OnUse (this.gameObject);
        }

        //
        // Summary:
        //     Sets the active item index
        // Parameter:
        //   activeItem
        //     The index of the active item
        public void SetActiveItem (int activeItem) {
            this.activeItem = activeItem;

            if (this.activeItem >= this.items.Count) {
                this.activeItem = this.items.Count - 1;
            }

            if (this.activeItem < 0) {
                this.activeItem = 0;
            }
        }

        //
        // Summary:
        //     Handles actions for the inventory
        private void Update () {
            if (Input.GetAxis ("Mouse ScrollWheel") > 0) {
                this.PrevItem ();
            }

            if (Input.GetAxis ("Mouse ScrollWheel") < 0) {
                this.NextItem ();
            }

            if (Input.GetButtonDown ("Fire1")) {
                this.UseActiveItem ();
            }
        }

        //
        // Summary:
        //     Selects the next item
        //     Jumps to the beginning if the last item is selected
        protected void NextItem () {
            this.activeItem++;

            if (this.activeItem >= this.items.Count) {
                this.activeItem = 0;
            }
        }

        //
        // Summary:
        //     Selects the previous item
        //     Jumps to the end if the first item is selected
        protected void PrevItem () {
            this.activeItem--;

            if (this.activeItem < 0) {
                this.activeItem = this.items.Count - 1;
            }
        }
    }
}