# UnityInventory
This project contains my first contact with the Untiy3d game engine... and it's a basic inventory system.

## Usage
- Put this repository somewhere in your Unity project folder
- Add the "Inventory (Script)" component to your Player (or whatever should have an Inventory)
- Assign an UI-Panel (For the inventory bar) and the UI-Item prefab to the Component
- Create a new item asset with Right-Click > Create > Item Base (or Sword if you like to)
- Add the Textures to the newly created item
- Drop an Item-Prefab into the world
- Assign the item asset to the "Pickup Item (script)" component
  - Optionally: Set the item count on the script (-1 = infinite)
- Have fun!

## Note
Feel free to use this script in your own games, fork it, improve it and send pull requests as I also will continue improving this project!
