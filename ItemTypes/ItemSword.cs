using System;
using UnityEngine;

namespace Inventory {
    [CreateAssetMenu]
    class ItemSword : ItemBase {
        public EnumToolMaterial material;

        public new ItemBase Copy () {
            ItemSword copy = (ItemSword) base.Copy ();

            copy.material = material;

            return copy;
        }

        public new void OnUse (GameObject player) {
            Debug.Log ("Used sword!");
        }
    }
}