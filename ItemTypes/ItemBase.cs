using UnityEngine;

namespace Inventory {
    [CreateAssetMenu]
    public class ItemBase : ScriptableObject {
        public Texture itemTexture;

        public Texture inWorldTexture;

        //
        // Summary:
        //     Creates a new instance of this item
        //     Used e.g. in the PickupItem component
        //
        // Returns:
        //     The newly created Item
        public ItemBase Copy () {
            ItemBase copy = ScriptableObject.CreateInstance<ItemBase> ();
            copy.itemTexture = this.itemTexture;
            copy.inWorldTexture = this.inWorldTexture;

            return copy;
        }

        //
        // Summary:
        //     Executes actions when the item is used
        // Parameters:
        //   player
        //     The player, that used the item
        public void OnUse (GameObject player) { }
    }
}