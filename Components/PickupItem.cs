using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Inventory {
    public class PickupItem : MonoBehaviour {
        //
        // Summary:
        //     The item, that the player should receive, when he runs through the GameObject
        public ItemBase item;

        //
        // Summary:
        //     How often the item should be given to players
        //     Set to -1 to have inifinite
        public int itemCount = -1;

        //
        // Summary:
        //     Sets the Items texture to the MeshRenderers Material
        void Start () {
            Material newMaterial = new Material (this.GetComponent<MeshRenderer> ().material);

            newMaterial.SetTexture ("_MainTex", this.item.inWorldTexture);

            this.GetComponent<MeshRenderer> ().material = newMaterial;
        }

        //
        // Summary:
        //     Gives the provided Item to the player if the player walks into the game object
        private void OnTriggerEnter (Collider other) {
            if (this.itemCount == 0) Object.Destroy (this.gameObject);

            Inventory inv = other.gameObject.GetComponent<Inventory> ();

            if (inv == null || this.item == null) return;

            if (!inv.AddItem (this.item.Copy ())) return;

            if (this.itemCount == -1) return;

            this.itemCount--;

            if (this.itemCount == 0) Object.Destroy (this.gameObject);
        }
    }
}